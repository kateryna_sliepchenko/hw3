const form = document.querySelector('.form');

form.addEventListener('submit', async (event) => {
    event.preventDefault();

    const formData = new FormData(form);
    const email = formData.get('email');
    const oldPassword = formData.get('');
    const newPassword = formData.get('password');
    const confirmPassword = formData.get('confirmPassword');

    if (newPassword !== confirmPassword) {
        alert('New password and confirmation doesn\'t equal');
        return;
    }

    const response = await fetch('/api/users/me/password', {
        method: 'PATCH',
        mode: 'cors',
        cache: 'no-cache',
        credentials: 'same-origin',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            oldPassword,
            newPassword
        })
    });

    if (response.status === 200) {
        alert('Passwords changed');
        location.href = '/';
    } else {
        alert('Data is incorrect!');
    }
})