const form = document.querySelector('.form');

form.addEventListener('submit', async (event) => {
  event.preventDefault();
  
  const formData = new FormData(form);
  const name = formData.get('name');
  const payload = formData.get('payload');
  const pickup_address = formData.get('pickup_address');
  const delivery_address = formData.get('delivery_address');
  const widthOfLoad = formData.get('width');
  const lengthOfLoad = formData.get('length');
  const heightOfLoad = formData.get('height');
  
  console.log(name);
  
  const response = await fetch('/api/loads', {
    method: 'POST',
    mode: 'cors',
    cache: 'no-cache',
    credentials: 'same-origin',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      name,
      payload,
      pickup_address,
      delivery_address,
      dimensions: {
        width: widthOfLoad,
        length: lengthOfLoad,
        height: heightOfLoad
      },
    })
  });
  
  
  // if (response.status === 200) {
  //   location.href = '/';
  // } else {
  //   alert('Data is incorrect!');
  // }
  
  console.log(response);
})