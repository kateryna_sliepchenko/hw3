const form = document.querySelector('.form');

form.addEventListener('submit', async (event) => {
    event.preventDefault();

    const formData = new FormData(form);
    const email = formData.get('email');

    const response = await fetch(`/api/auth/forgot_password`, {
        method: 'POST',
        mode: 'cors',
        cache: 'no-cache',
        credentials: 'same-origin',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            email
        })
    });

    if (response.status === 200) {
        alert('New password sent to your email');
        location.href = '/';
    } else {
        alert('Data is incorrect!');
    }
})