const form = document.querySelector('.form');

form.addEventListener('submit', async (event) => {
    event.preventDefault();

    const formData = new FormData(form);
    const email = formData.get('email');
    const role = formData.get('role');
    const password = formData.get('password');
    const confirmPassword = formData.get('confirmPassword');

    if (password !== confirmPassword) {
        alert('Password and password confirmation should be equal!');
        return;
    }

    const response = await fetch('/api/auth/register', {
        method: 'POST',
        mode: 'cors',
        cache: 'no-cache',
        credentials: 'same-origin',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            email,
            role,
            password
        })
    });

    if (response.status === 200) {
        alert('Registration is successful');
        location.href = '/sign-in';
    } else {
        alert('Error!', response);
    }
})