const Logger = require('../../../utils/logger');
const Validator = require('../../../utils/validator');

const {Load} = require('../../../models/Load.js');

const STATES = ['En route to Pick Up',
    'Arrived to Pick Up',
    'En route to delivery',
    'Arrived to delivery'];

// GET: /api/loads -get loads
const findLoads = async (request, response) => {
    Logger.log('Getting my loads with filter');

    let {offset = 0, limit = 10, status} = request.query;

    limit = Math.min(limit, 50);

    const query = Load
        .find({
            status: {
                $in: status.split(',')
            }
        });

    const count = await query
        .clone()
        .count();

    const loads = await query
        .skip(offset)
        .limit(limit)
        .select(['_id', 'name', 'created_by', 'assigned_to', 'status', 'state', 'payload', 'pickup_address', 'delivery_address', 'dimensions']);

    Logger.log(`Found ${count} loads, sent ${limit} loads with ${offset} offset.`);

    return response
        .status(200)
        .json({
            offset,
            limit,
            count,
            loads
        });
};

// POST: /api/loads - Add Load for User
const createLoad = async (request, response) => {
    Logger.log('Creating a new load');

    Validator(request, response)
        .required('name', 'payload', 'pickup_address', 'delivery_address', 'dimensions');

    const {name, payload, pickup_address, delivery_address, dimensions} = request.body;

    const load = new Load({
        name,
        created_by: request.user.id,
        payload,
        pickup_address,
        delivery_address,
        dimensions,
    });

    const resp = await load.save()
        .then(saved => {
            if (saved) {
                Logger.log('Load created successfully');

                return response.status(200)
                    .json({message: 'Load created successfully'})
            } else {
                Logger.log('Load created failed');

                return response
                    .status(400)
                    .json({message: 'Something wrong!'})
            }
        })
        .catch(unexpectedErrorHandler(request, response));

    console.log(resp);
}

// GET: /api/loads/active
const getMyActiveLoads = async (request, response) => {
    Logger.log('Getting user\'s active loads');

    const load = await Load.findOne({
        assigned_to: request.user.id,
        status: 'NEW'
    })
        .catch(unexpectedErrorHandler(request, response));

    Logger.log(`Active load successfully found`);

    return response
        .status(200)
        .json({
            load
        });
}

//console.log(getMyLoads(status='POSTED'));
//как тестировать?

// DELETE: /api/loads/active
const deleteLoadsById = async (request, response) => {
    Validator(request, response)
        .required('id');

    Load.findByIdAndDelete(request.params._id)
        .then((result) => {
            response.json({message: 'deleted'});
        });


    Logger.log(`Deleting a load with id: ${id}`);

    await Load
        .findById(noteId)
        .deleteOne()
        .then(result => {
            if (result.deletedCount) {
                Logger.log('Deleting my user is successful');
                return response.status(200).json({message: 'Success'})
            } else {
                Logger.log('Nothing to delete');
                return response.status(400).json({message: 'Nothing to delete'})
            }
        })
        .catch(unexpectedErrorHandler(request, response));
}

// PATCH: /api/loads/active/state
const iterateNextState = async (request, response) => {
    Logger.log('Iterate to next state');

    return Load.find(request.params.state).update(request.params.state)
}


function unexpectedErrorHandler(request, response) {
    return (error) => {
        Logger.error(`Unexpected error! ${error}`);

        response
            .status(500)
            .send({'message': `Unexpected error! ${error}`});
    };
}

module.exports = {
    createLoad,
    findLoads,
    getMyActiveLoads
}