const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

const Logger = require('../../../utils/logger');
const Validator = require('../../../utils/validator');
const {User} = require('../../../models/User.js');

const JWT_SECRET_KEY = process.env.JWT_SECRET_KEY;

const registerUser = async (request, response) => {
    Logger.log('Registering user');

    Validator(request, response)
        .required('email', 'password', 'role');

    const {email, password, role} = request.body;

    const user = new User({
        email,
        role,
        password: await bcrypt.hash(password, 10)
    });

    user.save()
        .then(saved => {
            Logger.log(`User successfully created`);

            return response.json({
                message: 'Profile created successfully',
                id: saved.id,
                email: saved.email,
                role: saved.role
            })
        })
        .catch(unexpectedErrorHandler(request, response));
}

const loginUser = async (request, response) => {
    Logger.log('Login user');

    Validator(request, response)
        .required('email', 'password');

    const user = await User.findOne({email: request.body.email})
        .select(['_id', 'email', 'role', 'password']);

    const {_id: id, email, role, password} = user;

    const passwordCorrect = await bcrypt.compare(String(request.body.password), String(password));

    if (user && passwordCorrect) {
        const payload = {id, email, role};
        const jwtToken = jwt.sign(payload, JWT_SECRET_KEY);

        Logger.log(`Authentication successful`);
        return response
            .cookie('authorization', jwtToken, {maxAge: 24 * 60 * 60 * 1000, httpOnly: true})
            .status(200)
            .json({jwt_token: jwtToken});
    } else {
        Logger.log(`Authentication failed`);
        return response
            .status(400)
            .json({'message': 'Not authorized'});
    }
}

const forgotPassword = async (request, response) => {
    Logger.log('Recovering user\'s password');

    Validator(request, response)
        .required('email');

    const user = await User.findOne({email: request.body.email})
        .select(['_id', 'email', 'role', 'password']);

    if (user) {
        user.password = await bcrypt.hash(String(Date.now()), 10);

        await user.save()
            .then(() => {
                Logger.log(`User's password successfully changed`);
            })
            .catch(unexpectedErrorHandler(request, response));

        //TODO: send email here
    } else {
        Logger.log('Recovering user\'s password is failed, user not exist');
    }

    return response
        .status(200)
        .json({message: 'New password sent to your email address'});
}

function unexpectedErrorHandler(request, response) {
    return (error) => {
        Logger.error(`Unexpected error! ${error}`);

        response
            .status(500)
            .send({'message': `Unexpected error! ${error}`});
    };
}

module.exports = {
    registerUser,
    loginUser,
    forgotPassword
}