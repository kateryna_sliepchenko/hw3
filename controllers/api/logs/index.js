// requires...
const fs = require('fs').promises;
const path = require('path');

const Logger = require('../../../utils/logger');

// constants...
const LOGS_PATH = path.join(__dirname, './../../logs/logs.txt');

// GET: /api/logs
function getLogs(request, response) {
    Logger.log('Getting logs');

    return fs.readFile(LOGS_PATH)
        .then(content => {
            Logger.log(`Logs was returned`);

            response
                .status(200)
                .send(content);
        })
        .catch((error) => {
            if (error.code === 'ENOENT') {
                response
                    .status(404)
                    .send({'message': `Logs not found`});
            } else {
                Logger.error(`Unexpected error! ${error}`);

                response
                    .status(500)
                    .send({'message': `Unexpected error! ${error}`});
            }
        });
}

module.exports = {
    getLogs
}