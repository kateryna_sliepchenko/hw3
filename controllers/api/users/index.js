const bcrypt = require('bcryptjs');

const Logger = require('../../../utils/logger');
const Validator = require('../../../utils/validator');

const {User} = require('../../../models/User.js');

// GET: /api/user/me
const getMyUser = async (request, response) => {
    Logger.log('Get my user');

    const user = await User
        .findById(request.user.id)
        .select(['_id', 'role']);

    if (user !== undefined) {
        Logger.log(`User was returned`);

        return response
            .status(200)
            .json({
                user
            });
    } else {
        unexpectedErrorHandler(request, response);
    }
};

// DELETE: /api/user/me
const deleteMyUser = async (request, response) => {
    Logger.log('Deleting my user');

    await User.findById(request.user.id)
        .deleteOne()
        .then(result => {
            if (result.deletedCount) {
                Logger.log('Deleting my user is successful');
                return response.status(200).json({message: 'Success'})
            } else {
                Logger.log('Nothing to delete');
                return response.status(400).json({message: 'Nothing to delete'})
            }
        })
        .catch(unexpectedErrorHandler(request, response));
};

// PATCH: /api/user/me
const updateMyUser = async (request, response) => {
    Logger.log('Patching my user');

    Validator(request, response)
        .required('email');

    const {email} = request.body;

    await User.findByIdAndUpdate(request.user.id, {
        email
    });

    Logger.log('Patching my user is successful');

    return response
        .status(200)
        .json({message: 'Success'});
};

// PATCH: /api/user/me/password
const updateMyPassword = async (request, response) => {
    Logger.log('Patching my user\'s password');

    Validator(request, response)
        .required('oldPassword', 'newPassword');

    const {oldPassword, newPassword} = request.body;
    const user = await User.findById(request.user.id);

    if (await bcrypt.compare(oldPassword, user.password)) {
        await User.findByIdAndUpdate(request.user.id, {
            password: await bcrypt.hash(newPassword, 10)
        });

        Logger.log('Patching my user\'s password is successful');

        return response
            .status(200)
            .json({message: 'Success'});
    } else {
        return response
            .status(400)
            .json({message: 'Something wrong!'});
    }
};

function unexpectedErrorHandler(request, response) {
    return (error) => {
        Logger.error(`Unexpected error! ${error}`);

        response
            .status(500)
            .send({'message': `Unexpected error! ${error}`});
    };
}

module.exports = {
    getMyUser,
    deleteMyUser,
    updateMyUser,
    updateMyPassword
};