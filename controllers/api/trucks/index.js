const bcrypt = require('bcryptjs');

const Logger = require('../../../utils/logger');
const Validator = require('../../../utils/validator');

const {Truck} = require('../../../models/Truck.js');

// POST: /api/truck/
const addTruckToUser = async (request, response) => {
  Logger.log('Creating a new truck');
  
  Validator(request, response)
    .required('assigned_to', 'type', 'status');
  
  const {assigned_to, type, status} = request.body;
  
  const truck = new Truck({
    created_by: request.user.id,
    assigned_to,
    type,
    status
  });
  
  const resp = await truck.save()
    .then(saved => {
      if (saved) {
        Logger.log('Truck created successfully');
        
        return response.status(200)
          .json({message: 'Truck created successfully'})
      } else {
        Logger.log('Truck created failed');
        
        return response
          .status(400)
          .json({message: 'Something wrong!'})
      }
    })
    .catch(unexpectedErrorHandler(request, response));
};

function unexpectedErrorHandler(request, response) {
  return (error) => {
    Logger.error(`Unexpected error! ${error}`);
    
    response
      .status(500)
      .send({'message': `Unexpected error! ${error}`});
  };
}

module.exports = {
  addTruckToUser
};