const jwt = require('jsonwebtoken');

const auth = (request, response, next) => {
    const {authorization} = request.cookies;

    if (!authorization) {
        return response
            .status(401)
            .json({'message': 'Please, provide authorization token'});
    }

    try {
        const tokenPayload = jwt.verify(authorization, process.env.JWT_SECRET_KEY);

        request.user = {
            id: tokenPayload.id,
            email: tokenPayload.email,
            role: tokenPayload.role
        }

        next();
    } catch (error) {
        return response
            .status(401)
            .json({message: error.message})
    }
}

const driver = (request, response, next) => {
    const {authorization} = request.cookies;

    if (!authorization) {
        return response
            .status(401)
            .json({'message': 'Please, provide authorization token'});
    }

    try {
        const tokenPayload = jwt.verify(authorization, process.env.JWT_SECRET_KEY);

        if (tokenPayload.role !== 'driver') {
            return response
                .status(401)
                .json({message: 'Available only for driver'})
        } else {
            request.user = {
                id: tokenPayload.id,
                email: tokenPayload.email,
                role: tokenPayload.role
            }

            next();
        }
    } catch (error) {
        return response
            .status(401)
            .json({message: error.message})
    }
}

module.exports = {
    authMiddleware: auth,
    driverMiddleware: driver
}