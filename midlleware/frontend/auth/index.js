const jwt = require('jsonwebtoken');

const guestMiddleware = (request, response, next) => {
    const {authorization} = request.cookies;

    if (authorization) {
        try {
            const tokenPayload = jwt.verify(authorization, process.env.JWT_SECRET_KEY);

            switch (tokenPayload.role) {
                case 'DRIVER':
                    response.redirect('/driver');
                    break;
                case 'SHIPPER':
                    response.redirect('/shipper');
                    break;
                default:
                    response.redirect('/500');
                    break;
            }
        } catch (error) {
            return response.redirect('/401');
        }
    } else {
        next();
    }
}

const authMiddleware = (request, response, next) => {
    const {authorization} = request.cookies;

    if (!authorization) {
        return response.redirect('/sign-in');
    } else {
        try {
            const tokenPayload = jwt.verify(authorization, process.env.JWT_SECRET_KEY);
            let redirectURL = '';

            switch (tokenPayload.role) {
                case 'DRIVER':
                    redirectURL = '/driver';
                    break;
                case 'SHIPPER':
                    redirectURL = '/shipper';
                    break;
                default:
                    redirectURL = '/500';
                    break;
            }

            if (request.baseUrl !== redirectURL) {
                response.redirect(redirectURL);
            } else {
                next();
            }
        } catch (error) {
            return response.redirect('/401');
        }
    }
}

module.exports = {
    authMiddleware,
    guestMiddleware
}