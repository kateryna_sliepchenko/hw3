require('dotenv').config();

const fs = require('fs');
const express = require('express');
const morgan = require('morgan');

const app = express();
const cookieParser = require('cookie-parser');
const mongoose = require('mongoose');

mongoose.connect(process.env.MONGO_URL);

const {authRouter: apiAuthRouter} = require('./routers/api/auth');
const {usersRouter: apiUsersRouter} = require('./routers/api/users');
const {loadsRouter: apiLoadsRouter} = require('./routers/api/loads');
const {truckRouter: apiTruckRouter} = require('./routers/api/trucks');
// const { logsRouter: apiLogsRouter } = require('./routers/api/logs');

const Logger = require('./utils/logger');
const {authMiddleware: feAuthMiddleware, guestMiddleware: feQGuestMiddleware} = require('./midlleware/frontend/auth');

app.use(express.json());
app.use(morgan('tiny'));
app.use(cookieParser());

app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
    next();
});

// API routes
app.use('/api/auth', apiAuthRouter);
app.use('/api/users', apiUsersRouter);
app.use('/api/loads', apiLoadsRouter);
app.use('/api/trucks', apiTruckRouter);
// app.use('/api/files', apiFilesRouter);
// app.use('/api/truck', truckRouter);
// app.use('/api/logs', apiLogsRouter);

// close some public files via middleware
app.use('/sign-in', feQGuestMiddleware);
app.use('/users/me/password', feAuthMiddleware);
app.use('/driver', feAuthMiddleware);
app.use('/shipper', feAuthMiddleware);
app.use('/user', feAuthMiddleware);

// access to static files
app.use(express.static(__dirname + '/public'));

// default page
app.use('/', (req, res) => res.redirect('/sign-in'));

const start = async () => {
    try {
        if (!fs.existsSync('storage')) {
            fs.mkdirSync('storage');
        }

        app.listen(process.env.APP_PORT);
        Logger.log('Application initialized');
    } catch (err) {
        console.error(`Error on server startup: ${err.message}`);
    }
};

start();

// ERROR HANDLER
app.use((error, request) => {
    console.error(error);
    request.status(500).send({message: 'Server error'});
});