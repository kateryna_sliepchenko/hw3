const mongoose = require('mongoose');

const Load = mongoose.model('Load', {
  name: {type: String,
  required: false},
  created_by: {
    type: String
  },
  assigned_to: {
      type: String,
      required: false
  },
  status: {
    type: String,
    enum: {
      values: ['NEW', 'POSTED', 'ASSIGNED', 'SHIPPED'],
      message: '{VALUE} is not supported.',
    },
    default: 'NEW',
  },
  state: {
    type: String,
    enum: {
      values: [
        'En route to Pick Up',
        'Arrived to Pick Up',
        'En route to delivery',
        'Arrived to delivery',
      ],
      message: '{VALUE} is not supported',
    },
    default: 'En route to Pick Up',
  },
  payload: {
    type: Number,
    required: false
  },
  pickup_address: {
      type: String,
      required: false
  },
  delivery_address: {
      type: String
  },
  dimensions: {
      type: Object,
      width: {
              type: Number,
          },
      length: {
              type: Number,
          },
      height: {
              type: Number,
          },
      required: true,
      
  },
  logs: {
    type: Array,
    message: String,
    time: Date,
    default: {
      message: 'Load is waiting to be assigned...',
      time: Date.now(),
    },
  },
  createdDate: {
    type: Date,
    default: Date.now()
  },
  updatedDate: {
    type: Date,
    default: Date.now()
  }
});

module.exports = {
    Load
}