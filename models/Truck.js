const mongoose = require('mongoose');

const Truck = mongoose.model('Truck', {
  created_by: {
      type: String,
      required: true,
  },
  assigned_to: {
      type: String,
  },
  type: {
    type: String,
    enum: {
      values: ['SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT'],
      message: '{VALUE} is not supported',
    },
    required: [true, 'Please provide a type of the truck'],
  },
  status: {
    type: String,
    enum: {
      values: ['OL', 'IS'],
      message: '{VALUE} is not supported',
    },
    default: 'IS',
  },
  createdDate: {
    type: Date,
    default: Date.now()
  },
  updatedDate: {
    type: Date,
    default: Date.now()
  }
});

module.exports = {
    Truck
};