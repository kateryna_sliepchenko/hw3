const mongoose = require('mongoose');

const User = mongoose.model('User', {
  email: {
    type: String,
    unique: true,
    required: [true, 'Please, provide an email']
  },
  role: {
    type: String,
    enum: {
      values: ['DRIVER', 'SHIPPER'],
      message: '{VALUE} is not supported',
    },
    required: [true, 'Please provide a role']
  },
  password: {
    type: String,
    required: [true, 'Please, provide a password']
  },
  createdDate: {
    type: Date,
    default: Date.now()
  },
  updatedDate: {
    type: Date,
    default: Date.now()
  }
});


module.exports = {
    User
};