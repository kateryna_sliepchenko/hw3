const fs = require('fs');
const path = require('path');

const LOGS_PATH = path.join(__dirname, './../../logs/logs.txt');

// check logger file, if not exist then create it
if (!fs.existsSync(LOGS_PATH)) {
    fs.mkdirSync(LOGS_PATH);
}

if (!fs.existsSync(LOGS_PATH)) {
    fs.writeFileSync(LOGS_PATH);
}

// open stream for update our log
const stream = fs.createWriteStream(
    LOGS_PATH,
    {flags: 'a'}
);

stream.on('error', (err) => {
    console.error(`Logger: ${err}`);
});

stream.on('finish', () => {
    console.log('Logger: done');
});

module.exports = {
    log: function (text) {
        const now = (new Date()).toISOString();

        console.log(`${now} - ${text}`);
        stream.write(`${now} - ${text}\n`);
    },

    error: function (error) {
        const now = (new Date()).toISOString();

        console.error(`${now} - Error: ${error}`);
        stream.write(`${now} - Error: ${error}\n`);
    }
};