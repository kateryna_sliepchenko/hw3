const express = require('express');

const index = express.Router();

const {getMyUser, deleteMyUser, updateMyUser, updateMyPassword} = require('../../../controllers/api/users');
const {authMiddleware} = require('../../../midlleware/api/auth');

index.get('/me', authMiddleware, getMyUser);
index.delete('/me', authMiddleware, deleteMyUser);
index.patch('/me', authMiddleware, updateMyUser);
index.patch('/me/password', authMiddleware, updateMyPassword);

module.exports = {
    usersRouter: index
};