const express = require('express');

const index = express.Router();

const {registerUser, loginUser, forgotPassword} = require('../../../controllers/api/auth');

index.post('/register', registerUser);
index.post('/login', loginUser);
index.post('/forgot_password', forgotPassword);

module.exports = {
    authRouter: index
};