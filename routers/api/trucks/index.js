const express = require('express');

const index = express.Router();

const { addTruckToUser } = require('../../../controllers/api/trucks');
const { authMiddleware } = require('../../../midlleware/api/auth');

// index.get('/', authMiddleware, getTrucks);
index.post('/', authMiddleware, addTruckToUser);
// index.get('/{id}', authMiddleware, getTruckById);
// index.put('/{id}', authMiddleware, updateLoadById);
// index.delete('/{id}', authMiddleware, deleteTruckById);
// index.post('/{id}/assign', authMiddleware, assignTruckById);

module.exports = {
  truckRouter: index
};