const express = require('express');

const index = express.Router();

const { createLoad, findLoads, getMyActiveLoads } = require('../../../controllers/api/loads');
const { authMiddleware, driverMiddleware } = require('../../../midlleware/api/auth');

index.get('/', authMiddleware, findLoads);
index.post('/', authMiddleware, createLoad);
index.get('/active', driverMiddleware, getMyActiveLoads);
// index.patch('/active/state', authMiddleware, iterateNextState);
// index.get('/{id}', authMiddleware, getUserLoadById);
// index.put('/{id}', authMiddleware, updateLoadById);
// index.delete('/{id}', authMiddleware, deleteLoadsById);
// index.post('/{id}/post', authMiddleware, postLoadById);//кажись я перепусала фенкциию реалтзации с первым

// index.get('/{id}/shipping_info');// в чем разница с другим?


module.exports = {
  loadsRouter: index
};