const express = require('express');
const index = express.Router();

const {getLogs} = require('../../../controllers/api/logs');

index.get('/', getLogs);

module.exports = {
    logsRouter: index
};